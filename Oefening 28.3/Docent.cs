﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._3
{
    internal class Docent
    {
        private string _naam;
        private List<Vak> _vakken = new List<Vak>();

        public Docent(string naam)
        {
            this.Naam = naam;
            Vakken = new List<Vak>();
        }

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        private List<Vak> Vakken
        {
            get { return _vakken; }
            set { _vakken = value; }
        }

        public void AddVak(Vak eenVak)
        {
            Vakken.Add(eenVak);
        }
        public void RemoveVak(Vak eenVak)
        {
            Vakken.Remove(eenVak);
        }
        public override string ToString()
        {
            string resultaat = $"{Naam} geeft de volgende vakken: " + Environment.NewLine;
            foreach (Vak vak in Vakken)
            {
                resultaat += vak.ToString() + Environment.NewLine;
            }
            return resultaat;
        }
    }
}
