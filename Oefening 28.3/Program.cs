﻿namespace Oefening_28._3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Docent docent = new Docent("Hans Van Soom");
            Vak vak1 = new Vak("Basis C#", 11, "LOK1");
            Vak vak2 = new Vak("Advanced C#", 13, "LOK1");
            Vak vak3 = new Vak("Web", 12, "LOK7");
            Vak vak4 = new Vak("GIT", 2, "LOK9");
            docent.AddVak(vak1);
            docent.AddVak(vak2);
            docent.AddVak(vak3);
            docent.AddVak(vak4);
            Console.WriteLine(docent.ToString());
            docent.RemoveVak(vak3);
            Console.WriteLine("Na het verwijderen van Web.");
            Console.WriteLine(docent.ToString());
        }
    }
}