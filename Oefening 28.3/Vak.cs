﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._3
{
    internal class Vak
    {
        private string _beschrijving;
        private string _leslokaal;
        private int _lesuren;

        public Vak(string beschrijving, int lesuren, string leslokaal)
        {
            this.Beschrijving = beschrijving;
            this.Lesuren = lesuren;
            this.Leslokaal = leslokaal;
        }

        public string Beschrijving
        {
            get { return _beschrijving; }
            set { _beschrijving = value; }
        }
        public string Leslokaal
        {
            get { return _leslokaal; }
            set { _leslokaal = value; }
        }
        public int Lesuren
        {
            get { return _lesuren; }
            set { _lesuren = value; }
        }

        public override string ToString()
        {
            return $"{Beschrijving} - {Lesuren} - {Leslokaal}";
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            //3 dingen doen:
            //1. Controleren of mijn object niet gelijk is aan null. COPY PASTE
            if (obj != null)
            {
                //2. Controleren of het datatype gelijk is aan elkaar. COPY PASTE
                if (GetType() == obj.GetType())
                {
                    //3. Eigen validatie
                    Vak eenVak = (Vak)obj;
                    if (this.Beschrijving == eenVak.Beschrijving)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
    }
}
